import React from 'react';
import './Copyright.css'
export const Copyright = (props) =>{
    return (
        <p className='copyright-dew'>Copyright © 2017 MOVIERISE. All Rights Reserved.</p>
    );
};
