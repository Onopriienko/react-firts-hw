import React from 'react';

import './Logo.css';

export const Logo = ({text1, text2, type}) => {
    const className = (type==='blue') ? 'logo-blue' : 'logo';
    const logoText1 = (text1) ? <span className='logo-text-bold'>{text1}</span> : null;
    const logoText2 = (text2) ? <span className='logo-text-regular'>{text2}</span> : null;

    return(
    <a className={className} href='/'>
        {logoText1}
        {logoText2}
    </a>
    );
};