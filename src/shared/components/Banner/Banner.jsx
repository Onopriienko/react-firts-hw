import React from 'react';
import {Button} from '../../../shared/components/Button';
import image from './MovieCollage.jpg';
import './Banner.css';




export const Banner =(props)=>{
    return(
        <div className ='banner-bckg' style={{backgroundImage: `url(${image})`}}>
                <div className='banner-wrapper'>
                    <p className='banner-title-text'>Receive information on the latest hit movies</p>
                    <p className='banner-title-text-second'>straight to your inbox.</p>
                    <div className='banner-button'>
                        <Button text ="subscribe!" type ='primary'/>
                    </div>
                </div>
        </div>
    );
};
