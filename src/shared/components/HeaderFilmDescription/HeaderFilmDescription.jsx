import React from 'react';
import {Button} from '../Button';
import './FilmDescription.css';

export const HeaderFilmDescription = (props) =>{
    const {name, genres, duration, rating} = props;
    const genresItems = genres.map(item=><li className='each-genre'>{item}</li>);
    const stars = Array(Math.round(rating)).fill(<span className='stars'>&#9733;</span>);
    return (
        <div className='film-description'>
            <h2 className='title-text'>{name}</h2>
            <div>
                <ul className='genres-nav'>
                    {genresItems}
                    <li className='separator'>|</li>
                    <span className="duration">{duration}</span>
                </ul>
            </div>
            <div className='stars-wrapper'>
                <div className='stars'>
                    {stars}
                    <span className='rating'>{rating}</span>
                </div>
                <div className='film-description-buttons'>
                    <Button text ="Watch Now" type ='primary'/>
                    <Button text ='View Info' type ='empty'/>
                    <Button text ='+ Favorites' type ='link'/>
                    <span><i className="fas fa-ellipsis-v"></i></span>
                </div>
            </div>
        </div>
    );
};

