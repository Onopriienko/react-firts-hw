import React from 'react';
import './SocialIconsMenu.css';

export const SocialIconsMenu = () =>{
    return (
        <div className='social-nav-wrapper'>
            <ul className='social-nav'>
                <li className='social-nav-item'><i className="fab fa-facebook-f"></i></li>
                <li className='social-nav-item'><i className="fab fa-twitter"></i></li>
                <li className='social-nav-item'><i className="fab fa-pinterest-p"></i></li>
                <li className='social-nav-item'><i className="fab fa-instagram"></i></li>
                <li className='social-nav-item'><i className="fab fa-youtube"></i></li>
            </ul>
        </div>
    );
};