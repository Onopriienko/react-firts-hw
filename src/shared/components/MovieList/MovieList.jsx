import React from 'react';
import {MovieCard} from "../MovieCard";
import './MovieList.css';

export const MovieList = ({list}) => {
    const fItem = list.map(elem=><MovieCard{...elem}/>);
    return (
        <div className="movie-card-wrapper">
            {fItem}
        </div>
    );
};
