import React from 'react';
import './Loading.css';


export const LoadingElement =(props)=>{
    return(
        <div className='loading-wrapper'>
            <i className="fas fa-ellipsis-h"></i>
            <p className="loading-text">Loading</p>
        </div>
    );
};