import React from 'react';
import './Button.css'

const buttonTypes = {
    primary: 'btn-primary',
    link: 'btn-link',
    empty: 'btn-empty'
};
export const Button = (props) =>{
    const {type, text} = props;
    const className = `btn ${buttonTypes[type]}`;
    return (
        <button className={className}>{text}</button>
    );
};
