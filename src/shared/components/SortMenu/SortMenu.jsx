import React from 'react';
import './SortMenu.css';


export const SortMenu =(props)=>{
    const {options} = props;
    const optionsItems = options.map(item=><li className='each-option'>{item}</li>);
    return(
        <div className="sort-menu-wrapper">
            <ul className='options-wrapper'>
                {optionsItems}
            </ul>
            <i className="fas fa-chevron-down"></i>
            <div className="view-options">
                <i className="fas fa-th-large large-1"></i>
                <i className="fas fa-th-large large-2"></i>
            </div>
        </div>
    );
};