import React from 'react';
import {Button} from '../Button';
import './AuthoristaionBar.css';

export const AuthorizationBar = (props) =>{
    const {authorizationBar} = props;
    return (
        <div className='authorization-bar'>
            <a className="search-logo"><i className="fas fa-search"></i></a>
            <Button text ="Sign in" type ='link'/>
            <Button text ='Sign up' type ='primary'/>
        </div>
    );
};