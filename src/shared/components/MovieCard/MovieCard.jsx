import React from 'react';
import './MovieCard.css';

export const MovieCard = (props) => {
    const {title, genre, rating, url} = props;
    return (
        <div className="movie-card">
                <img className='moviePicture' src={url}/>
                <div className="movie-card-info">
                    <div className="movie-card-info-main">
                        <p className="movie-title">{title}</p>
                        <p className="movie-genre">{genre}</p>
                    </div>
                    <div className="movie-card-info-rating">
                        <span className="movie-rating">{rating}</span>
                    </div>

                </div>
        </div>
    );
};
