import React from 'react';
import './Content.css';
import {SortMenu} from "../../../../shared/components/SortMenu";


export const Content =(props)=>{
    const {sortMenu, movieCardsList} = props;
    return(
        <section className ='main-section'>
            <SortMenu{...sortMenu}/>
        </section>
    );
};
