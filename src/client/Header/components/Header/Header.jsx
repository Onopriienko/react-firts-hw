import React from 'react';
import image from '../../../../shared/Images/Main-img.png';
import {Navbar} from '../../../Navbar/components/Navbar';
import {HeaderFilmDescription} from "../../../../shared/components/HeaderFilmDescription";
import './Header.css';


export const Header =(props)=>{
    const {navbar, headerFilmDescription} = props;
    return(
        <header className ='header' style={{backgroundImage: `url(${image})`}}>
            <Navbar {...navbar} />
            <HeaderFilmDescription{...headerFilmDescription}/>
        </header>
    );
};
