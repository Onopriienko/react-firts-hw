import React from 'react';
import './Footer.css';
import {FooterMenu} from "./../../../../shared/FooterMenu";
import {Logo} from "../../../../shared/components/Logo";
import {SocialIconsMenu} from "../../../../shared/components/SocialIconsMenu";
import {Copyright} from "../../../../shared/components/Copyright";


export const Footer = (props) => {
        const {logo, footerMenu} = props;
        return (
            <footer className='footer'>
                <div className='footer-wrapper'>
                    <div className='footer-row'>
                        <div className='footer-menu'>
                            <FooterMenu footerMenu={footerMenu}/>
                        </div>
                        <div className='logo-wrapper'>
                            <Logo {...logo}/>
                        </div>
                        <div className='icons-menu'>
                            <SocialIconsMenu/>
                        </div>
                    </div>
                    <div className='copyright'>
                        <Copyright/>
                    </div>
                </div>
            </footer>
        );
    };


