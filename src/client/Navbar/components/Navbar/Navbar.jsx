import React from 'react';

import {Logo} from '../../../../shared/components/Logo';
import {AuthorizationBar} from "../../../../shared/components/AuthorisationBar";


export const Navbar =(props)=>{
    const {logo, authorizationBar} = props;
    return(
        <div className="navbar">
            <Logo {...logo}/>
            <AuthorizationBar {...authorizationBar}/>
        </div>
    );
};