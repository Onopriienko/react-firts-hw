import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';



import {Header} from "../../../client/Header/components/Header";
import {Content} from "../../../client/Content/components/Content";
import {MovieList} from "../../../shared/components/MovieList";
import {Banner} from "../../../shared/components/Banner";
import {MovieList2} from "../../../shared/components/MovieListSecond";
import {LoadingElement} from "../../../shared/components/LoadingElement";
import {Footer} from "../../../../src/client/Footer/components/Footer";

export const App = () => {
    const pageProps = {
        navbar: {
            logo: {
                text1: 'movie',
                text2:'rise',
            },
        },
            headerFilmDescription:{
                name: 'The Jungle Book',
                genres: ['Adventure', 'Drama', 'Family', 'Fantasy'],
                duration: '1h 46m',
                rating: 4.8
            },
        sortMenu:{
            options:['Trending', 'Top Rated', 'New Arrivals', 'Trailers', 'Coming Soon', 'Genre' ]
        },
        movieCardsList:[
            {
                url:'https://i.ibb.co/r6XKknM/fantastic-beasts-and-where-to-find-them-teaser-by-umbridge1986-d7jek45-large-1-X.png" alt="fantastic-beasts-and-where-to-find-them-teaser-by-umbridge1986-d7jek45-large-1-X',
                title:'fantastic beasts',
                genre:'Adventure, Family, Fantasy',
                rating:'4.7'
            },
            {
                url:'https://i.ibb.co/sQf3tby/creed-1X.png',
                title:'assassin`s creed',
                genre:'Action, Adventure, Fantasy',
                rating:'4.2'
            },
            {
                url:'https://i.ibb.co/n7hMQqG/nowyouseeme-1-X.png',
                title:'now you see me',
                genre:'Action, Adventure, Comedy',
                rating:'4.4'
            },
            {
                url:'https://i.ibb.co/pPTkPxS/tarzan-1-X.png',
                title:'the legend of ta...',
                genre:'Action, Adventure, Drama',
                rating:'4.3'
            },
            {
                url:'https://i.ibb.co/VTb78yp/doctorstrange1-1-X.png" alt="doctorstrange1-1-X',
                title:'doctor strange...',
                genre:'Action, Adventure, Fantasy',
                rating:'4.8'
            },
            {
                url:'https://i.ibb.co/q5FD2st/maxresdefault-1-1-X.png',
                title:'captain america...',
                genre:'Action, Adventure, Sci-Fi',
                rating:'4.9'
            },
            {
                url:'https://i.ibb.co/sFmQMpn/Alice-Through-the-Looking-Glass-2016-film-poster.png',
                title:'alice through th...',
                genre:'Adventure, Family, Fantasy',
                rating:'4.1'
            },
            {
                url:'https://i.ibb.co/q74ZrJn/finding-dory-teaser-1-X.png',
                title:'finding dory',
                genre:'Animation, Adventure, Comedy',
                rating:'4.7'
            },
            {
                url:'https://i.ibb.co/H7L3cQX/the-bfg-new-poster-social-1-X.png',
                title:'the bfg',
                genre:'Adventure, Family, Fantasy',
                rating:'3.2'
            },
            {
                url:'https://i.ibb.co/3m7s989/independence-day-resurgence-1-X.png',
                title:'Independence Day',
                genre:'Action, Sci-Fi',
                rating:'3.9'
            },
            {
                url:'https://i.ibb.co/n7hMQqG/nowyouseeme-1-X.png',
                title:'Ice Age: Collisio...',
                genre:'Adventure, Comedy',
                rating:'4.5'
            },
            {
                url:'https://i.ibb.co/QPkhrL1/flex-moana-header-ddaba7de-1-X.png',
                title:'Moana',
                genre:'Action, Fantasy',
                rating:'4.9'
            },
        ],
        movieCardsList2:[
            {
                url:'https://i.ibb.co/q5FD2st/maxresdefault-1-1-X.png',
                title:'captain america...',
                genre:'Action, Adventure, Sci-Fi',
                rating:'4.9'
            },
            {
                url:'https://i.ibb.co/VTb78yp/doctorstrange1-1-X.png" alt="doctorstrange1-1-X',
                title:'doctor strange...',
                genre:'Action, Adventure, Fantasy',
                rating:'4.8'
            },
            {
                url:'https://i.ibb.co/sQf3tby/creed-1X.png',
                title:'assassin`s creed',
                genre:'Action, Adventure, Fantasy',
                rating:'4.2'
            },
            {
                url:'https://i.ibb.co/r6XKknM/fantastic-beasts-and-where-to-find-them-teaser-by-umbridge1986-d7jek45-large-1-X.png" alt="fantastic-beasts-and-where-to-find-them-teaser-by-umbridge1986-d7jek45-large-1-X',
                title:'fantastic beasts',
                genre:'Adventure, Family, Fantasy',
                rating:'4.7'
            },
        ],
        footer:{
            logo: {
                text1: 'movie',
                text2:'rise',
                type:'blue'
            },
            footerMenu: [
                {href:'#', text: 'About'},
                {href:'#', text: 'Terms of Service'},
                {href:'#', text: 'Contact'}
            ]
        },
    };
    const {footer} = pageProps;
    return (
        <div className="container">
            <Header{...pageProps}/>
            <Content{...pageProps}/>
            <MovieList list={pageProps.movieCardsList}/>
            <Banner/>
            <MovieList2 list={pageProps.movieCardsList2}/>
            <LoadingElement/>
            <Footer{...footer}/>
        </div>
    );
};